import pandas as pd
import pickle

def get_song_metrics(track_id, transformed_df):
    
    """returns the song data for a single song, in the style the model expects. Accepts a track_id as input"""
    
    # we need to use the same scaler we did when we trained
    with open("datafiles/MinMaxScaler.pkl", "rb") as file:
        scaler = pickle.load(file)
    
    # import and drop - exactly as we did before
    df = pd.read_csv("datafiles/SpotifyFeatures.csv")
    df = df.drop_duplicates(subset = "track_id")
    df.reset_index(drop = True)
    df_subset = df[df['track_id'] == track_id][:1]
    
    # this is the index of the song we're interested in, and the names of both song and artist
    index = df_subset.index
    song_name = df_subset.loc[0:,'track_name'].values[0]
    artist_name = df_subset.loc[0:,'artist_name'].values[0]

    # we need all the transformed metrics of that song, so we can find neighbors in a bit
    metrics = transformed_df.loc[index]
    
    return metrics, song_name, artist_name


def get_nearest_neighbours(song_metrics, df, recommender):
    
    """returns a list of tuples, one for each neighbor. tuple[0] is the song, and tuple[1] is the artist"""
    
    # get the neighbors
    songs = recommender.kneighbors(song_metrics)[1][0]
        
    # subset
    song_names = df.reindex(songs)['track_name']
    artists = df.reindex(songs)['artist_name']
            
    # zip songs with artists so they're pairs in tuples in a list
    neighbors = list(zip(song_names, artists))
    
    return neighbors[1:]