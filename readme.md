# Spotify song features and k-nearest neighbours

### Summary

Spotify songs have features such as "danceability", "key" and "acousticness". Using data like this we can cluster similar songs together, and recommend similar songs to one supplied.

The dataset originally had information on the artist, though this was dropped to prevent the model from over-using that feature (and because of memory errors when creating dummies)

### K-nearest neighbors
returns the 20 most similar songs to the supplied song. Works in a similar way to kmeans - represents all observations in many dimensions, and can returns the k nearest observations to the one you supply. The model will return the observation itself as the most near neighbour, so we're actually returning the 21 nearest neighbor, and ignoring the first.

### WORK IN PROGRESS